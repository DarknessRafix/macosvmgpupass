# macOS VM  + Single GPU Passthrough Guide

Since I changed my GPU to an incompatible one, no more updates will be provided to this guide. This method still works and is compatible even with macOS 15. Some things may be different, but you can figure it out. Good luck!

## Getting started
This guide is provided to help you install a VM with KVM/QEMU/Virt-manager.

I'll be referencing 2 projects:

- [ ] [ultimate-macOS-KVM](https://github.com/Coopydood/ultimate-macOS-KVM/)
- [ ] [Single GPU Passthrough](https://gitlab.com/risingprismtv/single-gpu-passthrough/-/wikis/home/)

## Important information

- This guide assumes you have need some basic knowledge

You need to feel confortable with the terminal, get into the BIOS to enable some emulation variables, and the most important: read and follow through the steps with caution.

- We need to use a GPU to get hardware acceleration

Unless we pass a GPU, the experience will be choppy and sluggish. Only some specific GPU's will work, and depending on your GPU you will have to use a specific macOS version. More on that later.

- Almost everything will be done using the scripts provided in ultimate-macOS-KVM

This will help us have as much of a pain-free experience as possible. This script is constantly getting updated with new features, it is possible that at the time of reading this it could have some extra features.
As far as I know, Dual GPU and Single GPU passthrough will be included in future versions. For now, we will do that step manually.

- macOS 12, 13 and 14 do have support

Installing a macOS VM breaks Apple ToS. We will be focusing on macOS 13, but the install will follow the same steps regarding on the version you're using. At the time of writing, macOS 14 has some bugs that make it harder to install (it's easy, but painful).
Remember that older versions do not have official support nor updates. Take that into account while installing. 
If we get a working setup with macOS 13 we can update it later to 14 without worries.
If you want to install macOS 14 from scratch, be sure to read the last steps on this guide.


## First steps

Before we did further, we need to check if our GPU is supported. That will be done using the script, which will provide more information about the steps you will have to follow on your GPU.
If you want to check it manually, you can follow this links.

- [ ] [AMD GPU](https://elitemacx86.com/threads/amd-gpu-compatibility-list-for-macOS.617/)

AMD GPU's are usually supported and are easy to set up.
Note: Navi 10 (RX 5xxx) and Navi 20 (RX 6xxx) are supported but need a boot argument. It will be setted up if needed later. Navi 30 (RX 7XXX) are not supported and will probably never will.

- [ ] [Nvidia GPU](https://elitemacx86.com/threads/nvidia-gpu-compatibility-list-for-macOS.614/)

Nvidia GPU's are only supported with older cards in older versions. If you have something newer (RTX) you're out of luck. As a last stand, check Open Core Legacy and see if there's something that you can use. I can't help as I don't have any Nvidia card.

For now, we will download the project and gather the info. Open a terminal and clone the repository wherever you like. We will run the provived python script.

```
git clone https://github.com/Coopydood/ultimate-macOS-KVM
cd ultimate-macOS-KVM
./main.py
```

The usage is simple. Type the number or letter of the command you want to use to go through the menus and press Enter. In this case, we will check for our GPU compatibility. So press 3 and Enter.

<img src="Compatibility.png" width="500"/></a>

From here, we will check our GPU. Press 3 and Enter. Then will ask for either an automatical detection or manual. Go for 1 and Enter.

<img src="GPU_Compatibility.png" width="500"/></a>

This will tell you which are the possible macOS versions that you will be able to run and some extra info about them. Write that info somewhere.
There's a possibility that it shows multiple GPU's like in my case, that detects my CPU integrated graphics. Just take into account your own external GPU.

If your GPU is not supported, you're out of luck and won't be able to continue.

## GRUB configuration

We have to change some configurations on GRUB in order to get everything working.
This boot parameters need to be added into GRUB. Take notes depending on your CPU brand.

| AMD CPU           | Intel CPU         |
|:-----------------:|:-----------------:|
| `amd_iommu=on`    | `intel_iommu=on`  |
| `iommu=pt`        | `iommu=pt`        |
| `video=efifb:off` |                   | 


Depending on your distribution, you will have to follow different steps. Do NOT copy this mindlessly. Check the lines and change it depending on your processor.


<details>
<summary><strong>Arch Linux, and most Arch based distros</strong></summary>

Run <code>sudo nano /etc/default/grub</code>

Edit the line that starts with <code>GRUB_CMDLINE_LINUX_DEFAULT</code> so it ressembles something like this, keeping any previous parameters if there is any:

<code>GRUB_CMDLINE_LINUX_DEFAULT="<strong>amd_iommu=on iommu=pt video=efifb:off</strong>"</code>

Update grub with the command <code>sudo grub-mkconfig -o /boot/grub2/grub.cfg</code>

</details>
<details> 
  <summary><strong>Ubuntu, Linux Mint, or any Debian based distros</strong></summary>

Run <code>sudo nano /etc/default/grub</code>

Edit the line that starts with <code>GRUB_CMDLINE_LINUX</code> so it ressembles something like this, keeping the previous parameters:

<code>GRUB_CMDLINE_LINUX_DEFAULT="<strong>amd_iommu=on iommu=pt video=efifb:off</strong> quiet splash"</code>

Update grub with the command <code>sudo update-grub</code>
</details>

<details>
<summary> <strong>Fedora, and Fedora based distros</strong> </summary>

Run <code>sudo nano /etc/default/grub</code>

Edit the line that starts with <code>GRUB_CMDLINE_LINUX</code> so it ressembles something like this, keeping the previous parameters:

<code>GRUB_CMDLINE_LINUX="resume=/dev/mapper/fedora_localhost--live-swap rd.lvm.lv=fedora_localhost-live/root rd.lvm.lv=fedora_localhost-live/swap <strong>amd_iommu=on iommu=pt video=efifb:off</strong> quiet"</code>

If you use Fedora 34 and up, update grub with the command <code>sudo grub2-mkconfig -o /boot/grub2/grub.cfg</code> 

If you use Fedora 33 or lower, update grub with the command <code>sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg</code>.
</details>

<details>
<summary><strong>Pop!_OS</strong> </summary>

Run <code>sudo nano /boot/efi/loader/entries/Pop_OS-current.conf</code>

Edit the line that starts with options so it ressembles something like this, keeping the previous parameters:

```
options root=UUID=211de945-3abe-4b4e-87f1-4ec1a062d9b6 ro quiet loglevel=0 systemd.show_status=false amd_iommu=on iommu=pt video=efifb:off splash
```

Update grub with the command <code>sudo bootctl update</code>
</details>

<details>
<summary><strong>Manjaro</strong></summary>

Run <code>sudo nano /etc/default/grub</code>

Edit the line that starts with <code>GRUB_CMDLINE_LINUX_DEFAULT</code> so it ressembles something like this, keeping the previous parameters:

<code>GRUB_CMDLINE_LINUX_DEFAULT="<strong>amd_iommu=on iommu=pt video=efifb:off</strong> quiet apparmor=1 security=apparmor udev.log_priority=3"</code>

Update grub with the command <code>sudo update-grub</code>
</details>

<details>
<summary><strong>OpenSUSE</strong></summary>

Run <code>sudo nano /etc/default/grub</code>

Edit the line that starts with <code>GRUB_CMDLINE_LINUX_DEFAULT</code> so it ressembles something like this, keeping the previous parameters:

<code>GRUB_CMDLINE_LINUX_DEFAULT="<strong>amd_iommu=on iommu=pt video=efifb:off</strong> splash=silent resume=/dev/disk/by-uuid/1652c07d-e2ba-4161-af2f-3e874eedfe1a mitigations=auto quiet"</code>

Update grub with the command <code>sudo grub2-mkconfig -o /boot/grub2/grub.cfg</code>

</details>
This changes will be effective on our next reboot. Now we will get into our BIOS, so turn off your machine and get there.


## BIOS configuration
Since every BIOS is different, you will have to do this step alone. We need to enable our platform to have virtualization. To do so, enable or disable the following parametwers on your BIOS.

Depending on your motherboard, you may or may not have every setting or could have a different name.

This are required in order to virtualize our CPU/GPU:
- IOMMU (AMD) or VT-D (Intel) -> Enabled
- NX Mode (AMD) or VT-X (Intel) -> True
- SVM mode (AMD) -> Enabled
- Secure boot -> Disabled

If your system is installed in UEFI, do NOT use CSM. Your distro has to be installed in UEFI so the GPU passthrough can work. Unless you explicitly changed it before, it should be in UEFI by default:
- CSM -> Disabled

This are needed in order to correctly pass our GPU. Not every motherboard has this parameters:
- Above 4G decoding -> Disabled 
- Resizable BAR Support -> Disabled


## IOMMU Groups

If you done that correctly, we now have to check our IOMMU groups to see if it lists our GPU correctly. Copy this on your terminal.

```
#!/bin/bash
shopt -s nullglob
for g in /sys/kernel/iommu_groups/*; do
    echo "IOMMU Group ${g##*/}:"
    for d in $g/devices/*; do
        echo -e "\t$(lspci -nns ${d##*/})"
    done;
done;
```

In that output you have to check for your graphics card. You have to check for an Graphics device and an audio device in IOMMU groups. Your output may look like this:

<details>
<summary><strong>AMD example</strong></summary>
We're looking for an RX 5700 XT in this case.

```
IOMMU Group 14:
	03:00.0 VGA compatible controller [0300]: Advanced Micro Devices, Inc. [AMD/ATI] Navi 10 [Radeon RX 5600 OEM/5600 XT / 5700/5700 XT] [1002:731f] (rev c1)
IOMMU Group 15:
	03:00.1 Audio device [0403]: Advanced Micro Devices, Inc. [AMD/ATI] Navi 10 HDMI Audio [1002:ab38]
```
</details>

<details>
<summary><strong>Nvidia example</strong></summary>
We're looking for a GTX 1060 in this case.

```
IOMMU Group 8:
        00:1f.0 ISA bridge [0601]: Intel Corporation B85 Express LPC Controller [8086:8c50] (rev 05)
        01:00.0 VGA compatible controller [0300]: NVIDIA Corporation GP106 [GeForce GTX 1060 6GB] [10de:1c03] (rev a1)
        00:1f.2 SATA controller [0106]: Intel Corporation 8 Series/C220 Series Chipset Family 6-port SATA Controller 1 [AHCI mode] [8086:8c02] (rev 05)
        00:1f.3 SMBus [0c05]: Intel Corporation 8 Series/C220 Series Chipset Family SMBus Controller [8086:8c22] (rev 05)
IOMMU Group 9:
        01:00.1 Audio device [0403]: NVIDIA Corporation GP106 High Definition Audio Controller [10de:10f1] (rev a1)
```
</details>
The perfect output shoud look something like in the AMD example with IOMMU Group 14 and 15 or Nvidia's Group 9 , with just 1 device per IOMMU group. If there's more devices per group, you will need to add them all. 

Additionally, we have to check for our USB headers. This is a must in order to get proper audio out on headsets and pass our mouse and keyboard. Check for anything that throws a USB controller message. In my case I have multiple.

<details>
<summary><strong>USB controllers</strong></summary>
´´´
IOMMU Group 27:
	0d:00.3 USB controller [0c03]: Advanced Micro Devices, Inc. [AMD] Device [1022:15b6]
IOMMU Group 28:
	0d:00.4 USB controller [0c03]: Advanced Micro Devices, Inc. [AMD] Device [1022:15b7]
´´´
</details>

Copy all this information and store it somewhere, but remove all the "bridge" devices if they appear in those groups, as those won't be needed.

## Software installation

Now we will install the software to run our VM's. Install this packages.

<details>
<summary><strong>Arch Linux and other Arch based distros</strong></summary>

```
sudo pacman -S virt-manager qemu vde2 ebtables iptables-nft nftables dnsmasq bridge-utils ovmf
```
Please note: Conflicts may happen when installing these programs.

A warning like the below example may apear in your terminal:
```
iptables and iptables-nft are in conflict. Remove iptables? [y/N]
```
If you do encounter this kind of message, press y and enter to continue the installation.
</details>

<details>
<summary><strong>Ubuntu, Linux Mint, Pop!_OS, and other Debian based distros</strong></summary>

```
sudo apt install qemu-system-x86 libvirt-clients libvirt-daemon-system libvirt-daemon-config-network bridge-utils virt-manager ovmf
```
</details>

<details>
<summary><strong>Fedora and other Fedora based distros</strong></summary>

```
sudo dnf install @virtualization
```
If you are following the Fedora part of this guide, take some time to check this [link](https://fedoramagazine.org/full-virtualization-system-on-fedora-workstation-30/). It will help you going forward.
</details>

<details>
<summary><strong>OpenSuse</strong></summary>
```
sudo zypper in libvirt libvirt-client libvirt-daemon virt-manager virt-install virt-viewer qemu qemu-kvm qemu-ovmf-x86_64 qemu-tools
```
</details>

## Libvirt configuration

Now we can start configuring libvirt to work with our user.

We need to edit some files.
```
sudo nano /etc/libvirt/libvirtd.conf
```
We need to find this 2 lines within the file and uncomment them (<code>#</code>)
```
unix_sock_group = "libvirt"
```
```
unix_sock_rw_perms = "0770"
```

Then we have to add our user to the libvirt group so it has permissions to run virtual machines. You can copy this code to do so automatically:
```
sudo usermod -a -G kvm,libvirt $(whoami)
```
You can verify if your user is on kvm and libvirt groups if the output of this command lists them:
```
sudo groups $(whoami)
```

Then we have to enable the service. We can either launch it manually everytime we boot the system or autostart it.

Autostart is done with:
```
sudo systemctl enable libvirtd
sudo systemctl start libvirtd
```
If you want to start the service manually with every boot, don't use the enable command, just use the start one.


Now we have to modify qemu.conf
```
sudo nano /etc/libvirt/qemu.conf
```

Search for this lines and uncomment them
```
user = "root"
```

```
group = "root"
```
You will need to replace <code>root</code> with your username, so they look like this

```
user = "myuser"
```

```
group = "myuser"
```

Then restart our libvirt daemon to load our new configs
```
sudo systemctl restart libvirtd
```

Our last step before installing will starting the virtual machine default network. Again, we can autostart with every boot or enable it manually.

Automatic with every boot:
```
sudo virsh net-autostart default
```

Manually:
```
sudo virsh net-start default
```

## ultimate-macOS-KVM's Autopilot

Now we can proceed and check if our system is ready to install the virtual machine. To do so, we'll go back into ultimate-macOS-KVM's <code>main.py</code> script.

As you already know how to go through the menus I won't be refering to the numbers anymore.

Go to Compatibility checks -> Basic KVM system check.

This will check if our system is ready for the install and will tell you if there's some steps that you're missing. If that does happen, please read the guide again and check if there's any step that you're missing.

<strong>NOTE:</strong> If you have chosen to use the manual start of the libvirtd service, the daemon will show as disabled. Do not worry about it.

<img src="ResultsCheck.png" width="500"/></a>

Now we can start the script once again to launch the Autopilot. Proceed with the default values until you reach <code>Select target OS</code>. Here you will have to<strong> select the latest macOS version that your GPU is capable of running</strong>.

Select the default number of <code>CPU cores</code>, <code>threads</code>, <code>CPU model</code>, and <code>CPU feature arguments</code>.

Select the amount of RAM that you will use for your virtual machine. As a rule of thumb, don't use more than half of your total host memory and use multiples of two. To type it, select <code>Custom value</code> and then the amount of GB with a <strong>G</strong>, for example <code>8GB</code>.

<strong>NOTE: Don't go with anything fancy like 3,5 or 7GB. macOS won't boot with those values.</strong>

Next is the virtual disk size. This will create a file that can grow up to the size you define in a qcow2 format. You can either use a file (the easiest) or setup a physical disk (won't cover that in this guide). If you have some experience with that, go for physical disk as it will bring the best performance.

Now we have to select the virtual disk type. At the time of writing, this option <strong>does nothing</strong>. Newer versions of the script will have improvements. For now, select <code>HDD</code> and continue.

Select the default network adapter model and either use the default MAC adress or generate one automatically. Then proceed and on macOS Recovery image file go with <code>Download from Apple</code>.

Now you can select the resolution of your virtual machine. Select the size of your screen on <code>More resolutions</code>.

Now select <code>Generate and import XML</code> to add our configuration into Virt-manager automatically. After that, a screen should appear with the values that you provided.


<img src="Ready_Genfiles.png" width="500"/></a>

Check if everything is fine and start the autopilot. It will take care of everything else, this will take some minutes and we will have 90% of our virtual machine configured and ready to install macOS.

<img src="Success.png" width="500"/></a>

Now select <code>Import XML</code> and enter your password. This will make the file usable in Virt-manager.

There's one step left which involves changing the boot parameters whithin the macOS bootloader. We need to launch the script one more time. Go to <code>Extras > macOS Boot Argument Editor</code>.

Here we have the option to AutoPatch some preset boot arguments, view or change, or restore to the default ones. First of all, we will go to <code>View or Change</code>. We should have this boot arguments
```
-v keepsyms=1 tlbto_us=0 vti=9
```

Copy the boot arguments without the -v. That enables <code>verbose</code> on boot and prevents from launching on some versions. Continue with <code>Enter new boot parameter</code> and paste this. Then <code>Apply</code>.
```
keepsyms=1 tlbto_us=0 vti=9
```

If your specific GPU needed a boot parameter, you can get an autoparched boot parameter by going into the same menus and clicking now in Autopatch. There's some default options for AMD GPU's.

We won't need anything else from the scripts. From now on we'll focus on Virt-manager.

## OSX Install
Go to Virt-manager to see our new fresh configuration. Before we can boot it we need to change one more thing. The CPU is incorrectly set and will not boot into macOS.

<img src="Mc1.png" width="500"/></a>

Click on <code>Open</code> and then go for <code>CPU</code>. Untick everything in this but "Copy host CPU configuration". You need to set up the cores as close as an actual system. If you remember about how we setted up our RAM, anything outside of multiples of 2 will crash, so don't get fancy with 7 cores. Different macOS versions require different minimum CPU cores.

<img src="Mc2.png" width="500"/></a>


Once you have that setted up, start the VM. You will find yourself in OpenCore, the bootloader for macOS:

<img src="Mc8.png" width="500"/></a>


Press Enter and get into the recovery tool. Then press on recovery and format the partition that has the size you decided. Just go with the default one and name it whatever you'd like.

<img src="Mc4.png" width="500"/></a>

Press on the top left to go back. Then into "Reinstall" and select the volume you just formated. By Apple terms of service, you're breaking the EULA by running a VM. You decide if you want to.

<img src="Mc5.png" width="500"/></a>

This will take up to 30 or 40 minutes, so grab a drink and get close to your screen in case something pops up.

Keep pressing on continue and selecting whatever options you decide. <strong>DON'T LOG INTO ANY APPLE ACCOUNT</strong>, you could get in trouble with how the VM is configured, but I won't get into detail about how to do it. You can follow this [link](https://github.com/Coopydood/ultimate-macOS-KVM/wiki/OpenCore), but you will need a working system before anything else.

Your VM will restart once or twice until you get to OpenCore once again. Select the new drive that shows up, this will get you to the post install and the desktop.

After you get into macOS, you can shut down from within the virtual machine and get to the GPU passthrough steps.

##Kexts to use depending on your GPU

There are 3 kexts (explained a little bit more below) that we have to use depending on your graphics card.
Almost every card will use the default one, which is WhateverGreen. Our OpenCore image comes with it already bundled so you don't have to do anything and can continue to the GPU passthrough.

If you a RX 6700, RX 6700 XT, RX 6750, RX 6400 or RX 6500XT you will have to use [NootRX](https://github.com/ChefKissInc/NootRX/).

If you use a Vega iGPU, you have to use [NootedRed](https://github.com/ChefKissInc/NootedRed).

To enable this kexts you must follow the part at the bottom <code>Using OpenCore Configurator to add kexts</code>.

## Single GPU Passthrough

This is time to either fight god or become one. We have to pass our PCI USB headers and CPU. This will take some time and some tries.

If you had a Windows VM with a previous passthrough, you're off to the races. Do the same steps as you did on a previous VM install, but you will also have to get your USB headers passed to the VM.

You can do this with a dual GPU, but here we will focus on single GPU. 

Before we do anything, we'll make a copy. This way, we'll have a bootable system without a GPU and a bootable system with it.



Get into Virt-manager once again and left click on the macOS VM. Let's clone it. You can name it whatever you want, but let's go with "macOS-GPU".

<img src="Mc7.png" width="500"/></a>

Untick everything. This way, we will have 2 different configurations to launch macOS, with or without GPU in case something goes wrong.

The next step is going into the [Single GPU Passthrough wiki]((https://gitlab.com/risingprismtv/single-gpu-passthrough/-/wikis/6)-Preparation-and-placing-of-the-ROM-file) "6) Preparation and placing of the ROM file up to "8) Attaching the GPU to your VM" included.

Follow <strong>every single step</strong>. I can't explain it that better than what is there.

Once that's done, you need to change the name of the VM that the Single GPU passthrough script will use. 

To do so, lets go to this qemu file
```
sudo nano /etc/libvirt/hooks/qemu
```

Where it says <code>win10</code>, change it to <code>macOS-GPU</code> or whatever name you wanted to use.

Our last step will be adding our USB headers in order to get proper audio and a proper USB control. To do so, check your PCI Host Devices and try to guess which ones control your USB headers. It should be noted similar to this.

<img src="USBs.png" width="500"/></a>

Some of those are explicitly mentioned, as is, in my case, the USB 3.2 controller. The other ones do not have a descrption, but as we checked before we have the id of those PCI. We have to pass those too. Test one at a time, if it does not work or boot, reboot your machine, remove the one you tested then try the next.

If everything goes well, you will have completed the setup. From now on, if you're on macOS 12, 13 or 14 you can run security updates without installing newer versions. If your graphics card was capable of running macOS 14 and you're on macOS 12 or 13, now you can update from within your system without any trouble. 

You can do some extra steps in order to get some extra performance, modify the boot parameters on OpenCore, enable bluetooth and other stuff that are optional. That part of the guide is still WIP.

 And that's the end. Enjoy your new virtual machine!
 

## Using OpenCore Configurator to add kexts

In order to patch our system we have to configure OpenCore. Every setup is different, so keep that in mind that your configuration will not work on any other system. To configure OpenCore there are two different things we need to take into account, kext files (kernel extension) and config.plist files. The first one are additions, small extensions that get added into our boot sequence. The second is the main file to edit all the configuration in OpenCore and you should always make a backup of it in case anything goes wrong.

We can manage both of this in either Linux or macOS, but for now the guide will explain how to do it in macOS. The steps provided should work for either NootRX and NootedRed.

For demostration purposes, I will show you how enable NootRX, which fixes graphic cards RX 6700, RX 6700 XT, RX 6750, RX 6400 and RX 6500XT. [I will follow this guide to do so](https://elitemacx86.com/threads/how-to-enable-amd-radeon-rx-6700-rx-6700-xt-rx-6750-rx-6500-xt-and-rx-6400.1452/).

First of all get into the VM without passing your GPU and download [OpenCore Configurator](https://mackie100projects.altervista.org/download-opencore-configurator/). Once it's downloaded, open it up. It will say that comes from the internet and may not be safe, but just click open. Once it opens, we will have a new icon in the top bar

<img src="OpenCoreIcon.png" width="700"/></a>

By clicking on it we can mount our OpenCore disk file. From here we have to click on mount on one of the disks. It will ask for your password.

<img src="OpenCoreTop.png" width="500"/></a>

If it does not populate your OpenCore Configurator, click on the unmount button specified on the photo and mount the other. 

<img src="OpenCorePopulated.png" width="1000"/></a>

Now we have the basic setup to edit things. You can check on Finder that we have a mounted EFI disk.

<img src="EFI.png" width="500"/></a>

We're going to grab the [NootRX kext](https://drive.google.com/file/d/1M0DvdVv7s1B5QG7y9SyS_yaqNNvJXosf/view). This is a precompiled version I found online, otherwise you have to compile it from source. BE AWARE.
Click on the download button on the right, otherwise you will download the contents of the kext but not the kext itself.

Once we have the .kext file we need to go to EFI/OC/Kexts and paste it there. Now we have to add the kext within OpenCore.

Go to OpenCore Configurator and click on <code>Kernel</code>. From here, we have to click on <code>Scan/Browse</code> and look for that kext on the same path. Then click on <code>Open</code> to see the kext there.

<img src="Ticks.png" width="1000"/></a>

You need to disable WhateverGreen, as that's used for a different set of graphic cards. <strong>You DO NOT NEED TO DISABLE WHATEVERGREEN IN ANY OTHER CASE IF YOU'RE JUST ADDING KEXTS.</strong>

Click on the top left red button and click on save. Now, restart your VM with your graphics card and hope for the best!

Now you have a functioning graphics card and know how to add kexts!

## Masking your system and improve consistency

This steps are towards making our VM's look as much as a legit Mac machine as possible. There's some extra steps that can be done but this should be enough for our case. I recommend to do so if you plan on playing videogames or use special software that could track information about your system. In order to do so, we need to check Mac hardware that's closer to our actual setup. From what applications can gather, this are the usual checks:

- CPU string, amount of cores/threads and frequency
- GPU model, amount of RAM and compatible encoders and API's for it
- Screen that is connected to, resolution, refresh rate and rotation
- Disk size and type of disk
- Serial ID
- Mac model
- CPU detected model

In our cases, the default information looks like this:
- CPU: Intel Core Processor (Haswell, no TSX) 

  This string is detected with <code>sysctl -n machdep.cpu.brand_string</code>. It detects this value because QEMU uses a generic emulated CPU. We will have to change the one we emulate and the string of it, because that is not the structure that an Intel Mac should have. Fortunately, to fix this we just have to type a new name. This will be explained later on.

- GPU: Whatever you passed

  If you dont have a GPU, this may look like unknown and use the CPU to load a basic driver.

- Screen: Depends
  
  If you have a GPU connected, it will output whatever screens are connected to it and it's information. If you don't pass a GPU, it may show a qemu generic screen.

- Disk size: Whatever you decided

  There's a possibility some apps track the type of disk. By default it will show as HDD. It may also track the name of the disks we have mounted, one showing whatever name we made and the other being the OpenCore image called QEMU HARDDISK.

- Serial ID: C07W10D9JYVX

  This is going to be tracked by Apple, and maybe some other apps. By default we use a generic one that you can change. The following process will alter the serial ID so you could, in theory, use Apple applications without problems. I do not recommend using them, but you do you. When you sign in to a Mac with your Apple ID, it becomes associated via the serial number. When many people sign in to their unique Apple IDs on the same shared serial number it becomes a problem. <strong>DO NOT LOG IN WITH THE DEFAULT SERIAL ID.</strong>

- Mac model: Mac mini (2018)

  This can be changed with the click of a button with OpenCore. This is called SMBIOS.


So how do we start from here? We first need to look at the [existing Mac hardware with Intel x86 processors](https://en.wikipedia.org/wiki/List_of_Mac_models_grouped_by_CPU_type#Intel_x86). We have to look at the ones that resemble closer to our system in number of CPU cores, threads and amount of RAM. It's easier to look at the newer models and start from there. Also, we need to take into account that MacBooks and iMacs do use an integrated screen and some older models may not have Thunderbolt connection, which we use to fake the GPU connection. You can still use them if you want to. But take that into account. Also, some models do allow you to upgrade your RAM, while others have it soldered, as it happens with the disk size and so on.

If you have a lower amount of cores but a good chunk of RAM and want to make it look like another system, we can fake extra cores with QEMU. We cannot fake our RAM.

Now, check the differnt Mac hardware and decide which one you plan on using. Some personal recommendations without any warranty:
- [Mac mini (2018)](https://en.wikipedia.org/wiki/Mac_Mini#Space_Gray_Unibody_(2018)): It's the one that comes bundled with the script, so this is the one you're actually running by default. Has two configurations: Either 4 CPU cores (for a total of 4 threads) or 6 CPU cores (for a total of 6 threads or 12 threads) with 8GB of RAM (also avaliable with 16, 32 and 64GB). If you decide to use this one you won't have to tinker much. Has Thunderbolt and soldered RAM and disks. Uses a Coffee Lake CPU.

- [Mac mini (2014)](https://en.wikipedia.org/wiki/Mac_Mini#Unibody_(2010%E2%80%932014)): It's definetly an older version. It's pretty modest, from 2 up to 4 cores and 8 or 16 GB of RAM. But it's catch is to be used with older macOS versions for those who have an Nvidia GPU. Has Thunderbolt support. Latest supported version was macOS 12. I did not personally test it. Uses a Haswell CPU.

- [Mac Pro (2019)](https://en.wikipedia.org/wiki/Mac_Pro#Specifications_3): It's the one I'm currently running and the one I'd recommend if you have a lot of cores. Has different versions and is upgradable from the start. 8, 12, 16, 24 or 28 cores. It has 32 GB of RAM by default, but since it has upgradable RAM slots we can put as litte as 8GB or as much as we want up, to 768GB or 1.5TB of RAM depending on the model. It has PCI slots and Thunderbolt. It requires a bit of tinkering and for the time being I do NOT recommend going this route and will not provide any help, since this takes significantly more time to do so. I do recoomend taking any other of the routes that I showed. Uses a Cascade Lake Xeon W CPU.

If you use any of those Mac mini models, you won't need to take the CPU model into account, but you should copy the nº of cores/threads/RAM.

In my case I'm going with a Mac Pro (2018) version that has 8 cores, 16 threads and 16GB of RAM. In case you decide to go with anything else this is where you can find your [CPU codes](https://www.qemu.org/docs/master/system/qemu-cpu-models.html) to use with QEMU. This is done to enable our CPU with a set of features that specific version of macOS expects from it. Should work just fine for pretty much everything with the default one we have.

Get into Virt-manager and open your macOS entry to edit.

First, we need to go to <code>CPU's</code> and change to <code>Manually set CPU topology</code>. In my case I have decided to go with a version that supports 8 cores, 16 threads. To do so I leave the values as:

- Sockets: 1
- Cores: 8
- Threads: 2 (as it will make 8x2=16)

Now that we're here, we can also enable hyperthreading in AMD and Supervisor Mode Execution Protection on Intel. To do so, click on <code>XML</code>. Almost at the start of the file, you can see an entry for CPU that will look like this:

<img src="XMLEdit.png" width="500"/></a>


```
  <cpu mode="host-passthrough" check="none" migratable="on">
    <topology sockets="1" dies="1" clusters="1" cores="8" threads="2"/>
  </cpu>
```

Then we need to add the line feature <code>policy</code>, depending on AMD or Intel.

<strong>AMD</strong>
```
  <cpu mode='host-passthrough' check='none'>
    <topology sockets='1' dies='1' cores=8' threads='2'/>
    <feature policy='require' name='topoext'/>
  </cpu>
```

<strong>Intel</strong>
```
  <cpu mode='host-passthrough' check='none'>
    <topology sockets='1' dies='1' cores='8' threads='2'/>
    <feature policy='disable' name='smep'/>
  </cpu>
```

After that, go to the bottom of the file. You will find this default string:

<code><qemu:arg value="Haswell-noTSX,kvm=on,vendor=GenuineIntel,+invtsc,vmware-cpuid-freq=on,+ssse3,+sse4.2,+popcnt,+avx,+aes,+xsave,+xsaveopt,check"/></code>

If we require it, we can change Haswell-noTSX for the actual one we're going to use with our system. Remember that this step is not mandatory and will just enable some features that the system will think our CPU supports. Again, if you go with a Mac mini you can leave it default.

As noted before, change the Haswell-noTSX with the one of the CPU family your model will use. In my case it's a Cascade Lake CPU, so it will be <code>Cascadelake-Server-noTSX</code>.


Lastly, we have to create the string of the CPU. You must do this step, no matter what. The structure of a real Intel Mac goes like this:

<strong>Intel Core</strong>
```
Intel Core processors: Intel(R) Core(TM) i7-9750H CPU @ 2.60GHz
```
<strong>Intel Xenon</strong>
```
Intel Xeon processors: Intel(R) Xeon(R) W-3223 CPU @ 3.50GHz
```


Edit that string to match the CPU of the system we're faking. Then, if needed modify the default CPU architecture if needed and add <code>model_id=xxxx</code> right after <code>kvm=on</code> with the string of text of your CPU. This is an example of how it should look after modifiying it with the previously mentioned Xeon CPU:

<code><qemu:arg value="<strong>Cascadelake-Server-noTSX</strong>,kvm=on,<strong>model_id=Intel(R) Xeon(R) W-3223 CPU @ 3.50GHz</strong>,vendor=GenuineIntel,+invtsc,vmware-cpuid-freq=on,+ssse3,+sse4.2,+popcnt,+avx,+aes,+xsave,+xsaveopt,check"/></code>



Now try launching the VM. If for whatever reason it does not launch, copy the default qemu_arg that I provided previously and see if it works. If it does not, then check again your CPU cores and threads configuration and repeat the steps. Then open the terminal and check the string provided:

```
sysctl -n machdep.cpu.brand_string
```

<img src="BrandString.png" width="500"/></a>


If you came here because of "that game" and you have it installed on macOS previously, you have to remove any tracking or previous files that are on your system. I recommend doing a clean install of the game and removing some tracking files. Uninstall the game normally, then go to the next directories. To easily travel within them, go to Finder, then Command+Shift+G (or look at the top bar and  Go > Go to folder). Find the next directory.

```
~/Library/Application Support
```

And remove anything related to it. In those folders there's a <code>machine.cfg</code> file that tracked your previous system settings with a hardware ID, so it's better to remove it. Everytime you modify anything that is related to how the hardware is emulated, I recommend deleting that file before doing anything else.